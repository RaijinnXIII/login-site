// Create form object
var signup_form = {
  username: "",
  email: "",
  password: "",
  confirm_password: ""
}

function loadFormValues(f) {
  f.username = document.getElementById('inputName').value;
  f.email    = document.getElementById('inputEmail').value;
  f.password = document.getElementById('inputPass').value;
  f.confirm_password  = document.getElementById('inputPassConfirm').value;
  return (f);
}

function validateUsername(f) {
  console.log(f.username);
  // 0. Cannot be empty
  
  // 1. must be 8 characters
  //
  // 2. must not be taken
}

// Submit button handler
function submitForm() {
  // Get user input
  var user_input = getFormInput(signup_form);
  console.log(user_input);
  // Basic Validation
}

// Export as module

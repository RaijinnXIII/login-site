function isEmptyString(s) {
  if (!s) {
    return true;
  } else {
    return false;
  }
}

function isUsernameLength(s, len=12) {
  var chars = s.split('');
  if ( chars.length <= len && !isEmptyString(s) ) {
    return true;
  }
  else if (isEmptyString(s)) {
    throw "[Input Error]: Username field cannot be empty."
  }
  else {
     throw "[Input Error]:  Username must be within 12 characters or less."
  }
}

function isPasswordLength(p, len=16) {
  var chars = p.split('');
  if (chars.length <= len) {
    return true;
  }
  else {
    return false;
  }
}

function passwordsMatch(p1,p2) {
  if ( p1 == p2 ) {
    return true;
  }
  else {
    return false;
  }
}

function existsInList(list, input) {
  for (i = 0; i <= list.length; i++) {
    if (list[i] == input) {
      return true;
    }
    else {
      continue;
    }
  }
  return false;
}

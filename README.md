![login-site](https://cloud.githubusercontent.com/assets/5169776/23390076/d4e443d4-fd39-11e6-8a66-120ff91ec5f8.png)
# login-site
Front and back-end signup and login logic.

## Tasks
- Browserify all components and funnel them into main.js
- Make submit button available to main.js to execute form handling and validation

## Sign Up logic


## Login logic

## Packages In Use
- NodeJS
- Browserify
- Live-Server
